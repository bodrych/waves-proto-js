import net from 'net'
import _ from 'lodash'
import { contentId, contentIdNames, Handshake, Message, Peers } from 'waves-proto-js'

// some known peers
const knownPeers = [
  { host: '35.158.218.156', port: 6868 },
  { host: '168.119.182.108', port: 6868 },
  { host: '116.202.14.9', port: 6868 },
  { host: '168.119.116.189', port: 6868 },
  { host: '135.181.87.72', port: 6868 },
  { host: '135.181.84.190', port: 6868 },
  { host: '52.48.34.89', port: 6868 },
]

// try to connect to random known peer
const client = net.createConnection(_.sample(knownPeers))

client.on('ready', () => {
  // say hello
  const handshake = new Handshake()
  client.write(handshake.toBuffer())

  // ask for known peers
  const getPeersMessage = new Message({ contentId: contentId.getPeers })
  client.write(getPeersMessage.toBuffer())
})

client.once('data', data => {
  console.log(data)
  console.log(Handshake.fromBuffer(data))

  let buffer = Buffer.from([])
  let targetLength = 0

  client.on('data', data => {
    if (buffer.length === 0) targetLength = data.readInt32BE() + 4
    buffer = Buffer.concat([buffer, data])
    if (buffer.length < targetLength) return
    // try to unmarshal message
    const targetBuffer = buffer.slice(0, targetLength)
    buffer = buffer.slice(targetLength)
    if (buffer.length >= 4) targetLength = buffer.readInt32BE() + 4
    const message = Message.fromBuffer(targetBuffer)
    console.log(contentIdNames[message.header.contentId], targetBuffer)
    switch (message.header.contentId) {
      // node asks for known peers
      case contentId.getPeers:
        // just reply with empty peers
        const peers = new Message({
          contentId: contentId.peers,
          payload: new Peers(),
        })
        client.write(peers.toBuffer())
        break
      // node sends known peers
      case contentId.peers:
        break
    }
  })
})

client.on('error', error => {
  console.log(`Error: ${error}`)
})

client.on('timeout', () => {
  client.destroy(new Error('timeout'))
})

client.on('close', () => {
  console.log('closed')
})